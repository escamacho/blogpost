function savePost(request){  
    // Mandamos a Guardar la información
    request.map(function(article) {
        
        fetch("posts/import", {
        method: "POST",
        body: JSON.stringify({
            name: article.title,
            description: article.description
        }),
        headers: { 
            "Content-type" : "application/json",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
        })
        .then(response => response.text())
        .then(text => console.log(text));
    });

    
}

function importPost(API_URL_EXT){
    // Para probar el funcionamiento de esta funcion, consultamos un blog por defecto
    API_URL_EXT = 'http://api.mediastack.com/v1/news?access_key=1ce88077336301ed0609f11cc663206b&languages=en,-de';
    $response = fetch(`${API_URL_EXT}`)
    .then(response => response.json())
    .then(data => savePost(data.data)).finally(function() { showLoading(false); });
}

function showLoading(option){
    var spinner = document.getElementById("spinner");

    var p = document.getElementById('message-import-post');
    var alert = document.getElementById('message-import-alert');

    if(option===true){
        spinner.hidden = false;
        alert.classList.remove('alert-success');
        p.innerHTML = "Cargando...";
    }

    if(option===false){
        spinner.hidden = true;
        alert.classList.add('alert-success');
        p.innerHTML = "Importación Exitosa";   
    }
}


(function(){ 
    const btnImport = document.querySelector('#import-post-confirm');
    btnImport.addEventListener('click', (event) => {

        showLoading(true);
        //Mandamos la URL del del blog externo
        response = importPost($("#path").val());
        
    });
 })();