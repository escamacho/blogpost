function getPost(filter_date){
    
    filter_date = filter_date === null ? '' : filter_date;
    const API_URL = "/post/all/" + filter_date;
    $("#main-blog-container").empty();
    const HTMLResponse = document.querySelector("#main-blog-container");
    
    fetch(`${API_URL}`)
    
        .then(response => response.json())
        .then(articles => {
            if(articles.length <= 0){
                let title = document.createElement('h5');
                title.setAttribute("class","main-blog-container--message");
                title.innerHTML = '¡No se encontraron articulos para la fecha seleccionada!';
                HTMLResponse.appendChild(title);
            }else{
                articles.forEach(article => {
    
                    var date = new Date(article.created_at);
                    date = date.toLocaleString();
        
                    let card = document.createElement('div');
                    card.setAttribute("class", "card");
        
                    let card_header = document.createElement('div');
                    card_header.setAttribute("class", "card--header");
        
                    let title = document.createElement('h3');
                    title.setAttribute("class","main-blog-container--card--title");
        
                    title.innerHTML = `${article.name}`;
        
                    card_header.appendChild(title);
        
                    let card_body = document.createElement('div');
                    card_body.setAttribute("class", "card--body");
        
                    let date_posted = document.createElement('p');
                    date_posted.setAttribute("class", "main-blog-container--date-posted");
                    date_posted.innerHTML = 'Fecha de Publicación: ' + `${date}`;
        
                    let description = document.createElement('div');
                    description.setAttribute("class", "main-blog-container--card--description");
                    description.innerHTML = `${article.description}`;
        
                    card_body.appendChild(date_posted);
                    card_body.appendChild(description);
        
                    let card_footer = document.createElement('div');
                    card_footer.setAttribute("class", "card--footer");
        
        
                    card.appendChild(card_header);
                    card.appendChild(card_body);
                    HTMLResponse.appendChild(card);
                });
            }
        });
}

(function(){ 
    getPost(null);
    const filterDate = document.querySelector('#filter_date');
    filterDate.addEventListener('change', (event) => {
        getPost(event.target.value);
        console.log(event.target.value);
    });

 })();