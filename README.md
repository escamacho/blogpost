# BlogPost

Página Web para la publicación de articulos 

### Comenzando

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
	wampserver64 o Xampp con php y mysql
```



### Instalación

_Sigue en orden los siguientes pasos:_

_1. Clonar el repositorio_

```
git clone https://escamacho@bitbucket.org/escamacho/blogpost.git
```
_2. Crea la base de base de datos en MySql llamada blog-post_

_3. Crea el archivo .env (En el .env.example te dejo lo que debes copiar y pegar)_

_4. Ejecutar los siguientes comandos de composer:_

```
composer install
```

```
composer update
```

_5. Ejecutar los siguientes comandos en orden para las migraciones y seeders:_

```
php artisan migrate
```

_Creamos el usuario por defecto (User=> eva@admin.com, Password => admin) mediante la ejecución del siguiente seeder:_
```
php artisan db:seed --class=UserSeeder
```
_Creamos 10 post por defecto (Este seeder es opcional) mediante la ejecución del seeder del siguiente seeder_
```
php artisan db:seed --class=PostSeeder
```

_6. Corremos el proyecto_
```
php artisan serve
```