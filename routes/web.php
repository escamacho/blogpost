<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/post/all/{date?}', [App\Http\Controllers\PostController::class, 'getAll'])->name('post.all');


Auth::routes();

 // Posts
 Route::get('posts',[App\Http\Controllers\PostController::class, 'index'])->name('posts');
 Route::post('posts/create', [App\Http\Controllers\PostController::class, 'store'])->name('posts.add');
 Route::post('posts/import', [App\Http\Controllers\PostController::class, 'import'])->name('posts.import');

