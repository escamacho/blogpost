<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>BlogPost</title>
        <!-- Fonts -->
        <link rel="stylesheet" href="https://unpkg.com/@fortawesome/fontawesome-free@5.14.0/css/all.min.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&family=Inter:wght@300;500&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script src="https://cdn.ckeditor.com/ckeditor5/31.0.0/classic/ckeditor.js"></script>
        @stack('styles')
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg">
            
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <h2><a class="web-site-title" href="/">BlogPost</a></h2>
                
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                        @if (Auth::check())
                            <li class="nav-item">
                                <a class="nav-link" href="/posts" class="elementor-item">Mis Articulos</a>
                            </li>
                        @endif  
                        @if (!Auth::check())
                            <li class="nav-item">
                                <a class="nav-link" href="/login" class="elementor-item">Iniciar Sesión</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/register" class="elementor-item">Registrarse</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="/" class="elementor-item">Salir</a>
                            </li>
                        @endif
                    </ul>

                    <form class="form-inline my-2 my-lg-0">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item" >
                                <div class="form-group d-flex justify-content-center mr-5">
                                
                                    <label for="filter_date" class="col-form-label mr-2">Filtrar desde la Fecha:</label>
                                    <input class="form-control float-left" type="date" id="filter_date">                    

                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </nav>
        </header>

        <main>
            <div id="app">
                @yield('content')
            </div>
        </main>
        @yield('modal')
        @stack('modals')
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        
        @yield('scripts')
        @stack('scripts')
    </body>
</html>
