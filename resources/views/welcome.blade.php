@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/welcome.css') }}">
@endpush

@section('content')
    <section class="main-about-us-container">
        <div class="main-about-us-container--title">
            <h2>Lee mientras te tomas un descanso</h2>
            <p>¡Mantente informado de las últimas novedades ó únete a nuestra red de Publicadores!</p>
            <a class="btn main-about-us-container--button" href="/register">Unirme</a>
        </div>
        <div class="main-about-us-container--image"></div>
        <div class="main-about-us-container--social-media">
            <span class="icon icon-facebook"></span>
            <span class="icon icon-instagram"></span>
            <span class="icon icon-likedin"></span>
        </div>
    </section>

    <section>
        <div class="main-services-container--card">
            <div class="main-services-container--card--body">
                <div class="row align-items-center">
                    <div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-4"><div class="main-services-container--card--image"></div></div>
                    <div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-8">
                        <p>
                            Con los blogs de los publicadores podrás mantenerte al tanto de todas las novedades
                            y temas de interes, adicionalmente puedes compartir tus conocimientos y opiniones publicando
                            tus porpios articulos.
                        </p>
                        <p><strong>¡BlogPost une a la comunidad, BlogPost es una Familia!</strong></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        
        <h3 class="section--title">Nuevos Articulos</h3>
        <div  class="main-blog-container" id="main-blog-container">
          
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('js/welcome.js') }}"></script>
@stop