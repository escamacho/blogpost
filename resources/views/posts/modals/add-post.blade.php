{{-- Create tags modal --}}
<div id="add-post" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Crear Articulo</div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form id="add-post-form" name="add-post-form" method="post" action="{{ route('posts.add') }}">
                @csrf
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <label for="name" class="text-nowrap mr-2 mb-0">Nombre del Articulo:</label>
                                <input type="text" id="name" name="name" class="form-control mb-0">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="description">Descripcion:</label>
                                    <textarea class="text-editor" name="description" id="description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" id="add-post-confirm" class="btn" style="background-color:#62A4AF; color:white">Create Articulo</button>
                </div>
            </form>
        </div>
    </div> 
</div>
