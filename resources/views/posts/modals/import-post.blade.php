{{-- Create tags modal --}}
<div id="import-post" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Importar Articulos</div>
                <button type="button" class="close" onclick="location.reload()" data-dismiss="modal">&times;</button>
            </div>

            <form id="import-post-form" name="import-post-form">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <label for="path" class="text-nowrap mr-2 mb-0">Ruta del blog externo:</label>
                                <input type="path" id="path" name="path" class="form-control mb-0">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12" id="spinner" hidden>
                                <div class="d-flex justify-content-center">
                                    <div class="spinner-border" role="status">
                                    </div>
                                  </div>
                            </div>
                            <div class="col-12 text-center alert" id="message-import-alert" role="alert">
                                <p id="message-import-post"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="location.reload()" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="import-post-confirm" class="btn" style="background-color:#62A4AF; color:white">Importar</button>
                </div>
            </form>
        </div>
    </div> 
</div>
