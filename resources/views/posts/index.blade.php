@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/post.css') }}">
@endpush

@section('content')
    <section>
        <div class="row">
            <div class="col-8">            
                <h2 class="total-post"><i class="fas fa-newspaper"></i> <strong>Articulos Publicados:</strong> {{count($posts)}}</h2>
            </div>
            <div class="col-4">
                <button type="button" class="btn btn-new-post float-right" data-toggle="modal" data-target="#add-post">+ Nuevo Articulo</button>
                <button type="button" class="btn btn-import-post float-right" data-toggle="modal" data-target="#import-post">Importar Articulos</button>
            </div>
        </div>
        @include('layouts.flash_messages')
    </section>
    <section>
        @if(count($posts)<0)
            <h3 class="section--title">Aún no has publicado articulos, que espera?</h3>
        @else 
            <div  class="main-blog-container">
                @foreach ($posts as $post)
                    <div class="card">
                        <div class="card--header">
                            <h3 class="main-container--card--title">{{$post->name}}</h3>
                        </div>
                        <div class="card--body">
                            <p class="main-container--date-posted">Fecha de publicación: {{date('M j, Y', strtotime($post->created_at))}}</p>
                            <div>
                                @php
                                    $data =  $post->description;
                                    $data = Str::limit($data, 23, ' (...)')
                                @endphp
                                    {!! $post->description !!} 
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </section>
@endsection
@push('modals')
    @include('posts.modals.add-post');
    @include('posts.modals.import-post');
@endpush

@section('scripts')
    <script src="{{ asset('js/post.js') }}"></script>
    <script>
        ClassicEditor
                .create( document.querySelector( '.text-editor' ) )
                .then( editor => {
                        console.log( editor );
                } )
                .catch( error => {
                        console.error( error );
                } );
    </script>
@stop