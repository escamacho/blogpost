<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Auth;


class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => ['getAll']], );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $posts = POST::where('user_id',$user->id)->orderBy('created_at','DESC')->get();
        return view('posts.index', compact('posts'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll($date=null)
    {
        if(is_null($date)){
            $posts  = Post::orderBy('created_at','DESC')->get();
        }else{
            $posts  = Post::where('created_at','>=',$date)->orderBy('created_at','DESC')->get();
        }
        return $posts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $rules = [
            'name' => 'required|unique:posts|min:3',
            'description' => 'required',
        ];
        $request->validate($rules);

        $post = Post::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'user_id' => $user->id
        ]);
        return back()->with([
            'flash_message' => 'Articulo Creado!',
            'flash_message_important' => true,
            'flash_alert_type' => "success",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }

    /**
     * Store external API resources
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request){
        $user = Auth::user();
        Post::create([
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => $user->id
        ]);
        return $request;
    }
}
