<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $description = "<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting 
                        industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                        when an unknown printer took a galley of type and scrambled it to make a type 
                        specimen book. It has survived not only five centuries, but also the leap into 
                        electronic typesetting, remaining essentially unchanged. <br>
                        It was popularised in the 1960s with the release of Letraset sheets containing Lorem 
                        Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker 
                        including versions of Lorem Ipsum.<br></hr>
                        <strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting 
                        industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                        when an unknown printer took a galley of type and scrambled it to make a type 
                        specimen book. It has survived not only five centuries, but also the leap into 
                        electronic typesetting, remaining essentially unchanged. <br>
                        It was popularised in the 1960s with the release of Letraset sheets containing Lorem 
                        Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker 
                        including versions of Lorem Ipsum.<br><br>
                        <h5>Lorem Ipsum Title</h5>
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                        when an unknown printer took a galley of type and scrambled it to make a type 
                        specimen book. It has survived not only five centuries, but also the leap into 
                        electronic typesetting, remaining essentially unchanged. <br>
                        It was popularised in the 1960s with the release of Letraset sheets containing Lorem 
                        Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker 
                        including versions of Lorem Ipsum.<br>
                        <hr>
                        <span><strong>Link: </strong><a href='https://www.lipsum.com/' >https://www.lipsum.com/</a></span>
                        ";
        for ($i = 1; $i <= 10; $i++) {
            $post = new Post();
            $post->name = "Articulo " .$i;
            $post->description = "<strong>Description N°:" .$i ."</strong> " .$description ;
            $post->user_id = 1;
            $post->save();
        }
    }
}
