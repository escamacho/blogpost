<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::Create([
            'first_name' => 'Eva',
            'last_name' => 'Camacho',
            'email' => 'eva@admin.com',
            'age' => '40',
            'password' => bcrypt('admin'),
        ]);
    }
}
